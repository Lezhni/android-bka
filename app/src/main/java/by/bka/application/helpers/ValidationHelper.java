package by.bka.application.helpers;

import android.widget.EditText;

public class ValidationHelper {

    private static String errorTextEmpty = "Поле обязательно к заполнению и должно содержать минимум 2 символа.";
    private static String errorTextPhone = "Поле обязательно к заполнению и должно содержать минимум 7 символов.";

    public static boolean validateName(EditText field) {
        if (field.getText().toString().replaceAll("\\s","").length() < 2) {
            showError(errorTextEmpty, field);
            return false;
        }
        return true;
    }

    public static boolean validatePhone(EditText field) {
        String fieldValue = field.getText().toString().replaceAll("\\s","");
        if (fieldValue.length() < 7) {
            showError(errorTextPhone, field);
            return false;
        }
        return true;
    }

    private static void showError(String errorText, EditText field) {
        field.setError(errorText);
        field.requestFocus();
    }
}
