package by.bka.application;

import android.content.DialogInterface;
import android.content.Intent;
import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {

    private static final int PERMISSION_CALL = 100;

    /* list of titles/services for ServiceActivity */
    private static final SparseArray<String> SERVICES_LIST = new SparseArray<String>() {
        {
            append(R.id.evacuation, "Эвакуация автомобиля");
            append(R.id.gasoline, "Подвоз топлива");
            append(R.id.lock, "Вскрытие замков");
            append(R.id.wheel, "Замена колеса");
            append(R.id.battery, "Запуск двигателя от внешнего источника");
            append(R.id.additional, "Сопутствующие услуги");
        }
    };

    private DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        /* show menu icon on actionbar */
        ActionBar actionBar = getSupportActionBar();
        if (null != actionBar) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
        }

        /* services buttons */
        LinearLayout evacuationButton = findViewById(R.id.evacuation);
        evacuationButton.setOnClickListener(this);

        LinearLayout gasolineButton = findViewById(R.id.gasoline);
        gasolineButton.setOnClickListener(this);

        LinearLayout lockButton = findViewById(R.id.lock);
        lockButton.setOnClickListener(this);

        LinearLayout wheelButton = findViewById(R.id.wheel);
        wheelButton.setOnClickListener(this);

        LinearLayout batteryButton = findViewById(R.id.battery);
        batteryButton.setOnClickListener(this);

        Button crashButton = findViewById(R.id.crash);
        crashButton.setOnClickListener(this);

        LinearLayout additionalServicesButton = findViewById(R.id.additional);
        additionalServicesButton.setOnClickListener(this);

        /* navigation drawer */
        drawerLayout = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.sidebar_navigation);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        /* check is first app launch */
        SharedPreferences preferences = getSharedPreferences("by.bka.application", MODE_PRIVATE);
        if (preferences.getBoolean("appFirstLaunch", true)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder
                .setMessage(R.string.dialog_first_launch_text)
                .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    Intent profileIntent = new Intent(MainActivity.this, ProfileActivity.class);
                    startActivity(profileIntent);
                    }
                })
                .setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {}
                }).show();

            preferences.edit().putBoolean("appFirstLaunch", false).apply();
        }
    }

    @Override
    public void onClick(View v) {
        Intent serviceIntent;
        switch (v.getId()) {
            case R.id.additional:
                serviceIntent = new Intent(this, AdditionalServicesActivity.class);
                break;
            case R.id.crash:
                serviceIntent = new Intent(this, CrashActivity.class);
                break;
            default:
                serviceIntent = new Intent(this, ServiceActivity.class);
                serviceIntent.putExtra("serviceName", SERVICES_LIST.get(v.getId()));
        }
        startActivity(serviceIntent);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Intent menuIntent = null;
        switch (item.getItemId()) {
            case R.id.menu_terms_of_use:
                menuIntent = new Intent(this, InfoActivity.class);
                break;
            case R.id.menu_profile:
                menuIntent = new Intent(this, ProfileActivity.class);
                break;
            case R.id.menu_reviews:
                menuIntent = new Intent(Intent.ACTION_VIEW);
                menuIntent.setData(Uri.parse("market://details?id=by.bka.application"));
                break;
        }

        if (null != menuIntent) {
            startActivity(menuIntent);
        }

        drawerLayout.closeDrawers();
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            /* make a hot line call */
            case R.id.action_call:
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, PERMISSION_CALL);
                } else {
                    Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + getResources().getString(R.string.phone_116)));
                    startActivity(callIntent);
                }
                return true;

            /* toggle navigation drawer */
            case android.R.id.home:
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawers();
                } else {
                    drawerLayout.openDrawer(GravityCompat.START);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERMISSION_CALL) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + getResources().getString(R.string.phone_116)));
                    startActivity(callIntent);
                }
            }
        }
    }
}

