package by.bka.application;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.crashlytics.android.Crashlytics;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import by.bka.application.helpers.ServiceOrderHelper;
import by.bka.application.helpers.ValidationHelper;
import cz.msebera.android.httpclient.Header;
import io.fabric.sdk.android.Fabric;

public class CrashActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int PERMISSION_CALL = 100;

    private SharedPreferences profilePreferences;

    private Button submitButton;
    private EditText phoneEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_crash);

        phoneEditText = findViewById(R.id.input_crash_phone);

        submitButton = findViewById(R.id.action_submit);
        submitButton.setOnClickListener(this);

        Button callButton = findViewById(R.id.action_call);
        callButton.setOnClickListener(this);

        /* fill profile's field with data from preferences */
        profilePreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String profilePhone = profilePreferences.getString("profilePhone", "+375");
        phoneEditText.setText(profilePhone);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            /* make a fast call */
            case R.id.action_call:
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, PERMISSION_CALL);
                } else {
                    Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + getResources().getString(R.string.phone_116)));
                    startActivity(callIntent);
                }
                break;

            /* send an order */
            case R.id.action_submit:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);

                if (!ServiceOrderHelper.checkInternetConnection(this)) {
                    builder.setMessage(R.string.dialog_no_internet).show();
                    return;
                }

                /* the simplest validation */
                if (ValidationHelper.validatePhone(phoneEditText)) {
                    sendOrder();
                }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        /* permission to make call */
        if (requestCode == PERMISSION_CALL) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + getResources().getString(R.string.phone_116)));
                    startActivity(callIntent);
                }
            }
        }
    }

    private void sendOrder() {
        ServiceOrderHelper.disableSendButton(submitButton);

        String serviceName = getResources().getString(R.string.crash_service_text);
        final AlertDialog.Builder builder = new AlertDialog.Builder(CrashActivity.this);

        /* collect all request data to HashMap */
        RequestParams params = new RequestParams();

        params.put("service", serviceName);
        params.put("phone", phoneEditText.getText().toString());

        /* add fields from preferences */
        String carNumber = profilePreferences.getString("profileCarNumber", "");
        params.put("carNumber", carNumber);

        String profileName = profilePreferences.getString("profileName", "");
        params.put("name", profileName);

        Boolean profileMembership = profilePreferences.getBoolean("profileMembership", false);
        if (profileMembership) {
            String profileCardNumber = profilePreferences.getString("profileCardNumber", "");
            params.put("cardNumber", profileCardNumber);
        }

        ServiceOrderHelper.send(params, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                builder.setMessage(R.string.request_success).show();
                ServiceOrderHelper.enableSendButton(submitButton);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                builder.setMessage(R.string.request_fail + ". Текст ошибки: " + new String(errorResponse)).show();
                ServiceOrderHelper.enableSendButton(submitButton);
            }
        });
    }
}
