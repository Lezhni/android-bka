package by.bka.application.helpers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Button;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import by.bka.application.R;

public class ServiceOrderHelper {

    private static final String SEND_URL = "http://bka.by/app/send.php";
    private static AsyncHttpClient client = new AsyncHttpClient();

    /* send data */
    public static void send(RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.post(SEND_URL, params, responseHandler);
    }

    /* check is exist internet connection */
    public static boolean checkInternetConnection(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (null != cm) {
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            return netInfo != null && netInfo.isConnectedOrConnecting();
        }
        return false;
    }

    /* disable/enable submit button while sending data */
    public static void disableSendButton(Button submitButton) {
        submitButton.setText(R.string.hint_sending);
        submitButton.setEnabled(false);
    }

    public static void enableSendButton(Button submitButton) {
        submitButton.setText(R.string.hint_send);
        submitButton.setEnabled(true);
    }
}
