package by.bka.application;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import by.bka.application.helpers.AssetsHelper;
import by.bka.application.helpers.ServiceOrderHelper;
import by.bka.application.helpers.ValidationHelper;
import cz.msebera.android.httpclient.Header;
import io.fabric.sdk.android.Fabric;

public class ServiceActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_FROM_PLACE_PICKER = 1;
    private static final int REQUEST_TO_PLACE_PICKER = 2;
    /*private static final int PERMISSION_SMS = 100;*/

    private SharedPreferences profilePreferences;

    private String serviceName = null;
    private List<String> carsMakes = new ArrayList<>();

    private Button submitButton;
    private AutoCompleteTextView carMakeEditText;
    private EditText addressFromEditText, addressToEditText, nameEditText, phoneEditText, carModelEditText, descriptionEditText;
    private AppCompatSpinner transmissionTypeSpinner, clearWaySpinner, gasolineTypeSpinner, gasolineVariantSpinner, gasolineVolumeSpinner, voltageSpinner;
    private CheckBox blockedHelmCheckBox, blockedBrakeCheckBox, damagedWheelsCheckBox, spareWheelCheckBox, wheelSecretsCheckBox, existedAccessCheckBox, openingHoodCheckBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_service);

        /* set activity title (depend on selected service) */
        Intent serviceIntent = getIntent();
        if (serviceIntent.hasExtra("serviceName")) {
            serviceName = serviceIntent.getStringExtra("serviceName");
            setTitle(serviceName);
        }

        Button mapFromButton = findViewById(R.id.action_from_map);
        mapFromButton.setOnClickListener(this);

        Button mapToButton = findViewById(R.id.action_to_map);
        mapToButton.setOnClickListener(this);

        submitButton = findViewById(R.id.action_submit);
        submitButton.setOnClickListener(this);

        /* required inputs with data to send */
        addressFromEditText = findViewById(R.id.input_from_address);
        nameEditText = findViewById(R.id.input_name);
        phoneEditText = findViewById(R.id.input_phone);
        carMakeEditText = findViewById(R.id.input_make);
        carModelEditText = findViewById(R.id.input_model);
        transmissionTypeSpinner = findViewById(R.id.input_transmission_type);

        /* parse JSON with makes/models */
        String carsJSON = AssetsHelper.getJSONFile(this, "carsList.json");
        try {
            JSONArray carsMakesJSON = new JSONArray(carsJSON);
            for (int i = 0; i < carsMakesJSON.length(); i++) {
                JSONObject carMakeJSON = carsMakesJSON.getJSONObject(i);
                String carMakeName = carMakeJSON.getString("brand");
                carsMakes.add(carMakeName);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        /* fill AutoCompeleteEditText with cars' makes */
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, carsMakes);
        carMakeEditText.setAdapter(adapter);

        /* fill profile's field with data from preferences */
        profilePreferences = PreferenceManager.getDefaultSharedPreferences(this);

        String profileName = profilePreferences.getString("profileName", "");
        nameEditText.setText(profileName);

        String profilePhone = profilePreferences.getString("profilePhone", "+375");
        phoneEditText.setText(profilePhone);

        String profileCarMake = profilePreferences.getString("profileCarMake", "");
        carMakeEditText.setText(profileCarMake);

        String profileCarModel = profilePreferences.getString("profileCarModel", "");
        carModelEditText.setText(profileCarModel);

        String profileCarTransmission = profilePreferences.getString("profileCarTransmission", "Механическая");
        ArrayAdapter<String> transmissionTypesAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, getResources().getStringArray(R.array.transmissionTypes));
        int profileCarTransmissionPosition = transmissionTypesAdapter.getPosition(profileCarTransmission);
        transmissionTypeSpinner.setSelection(profileCarTransmissionPosition);

        /* show fields and init inputs only for current service */
        TextView noticeLabel = findViewById(R.id.notice_label);

        if (null != serviceName) {
            if (serviceName.equals("Эвакуация автомобиля")) {
                LinearLayout fieldsEvacuation = findViewById(R.id.fields_group_evacuation);
                fieldsEvacuation.setVisibility(View.VISIBLE);
                descriptionEditText = findViewById(R.id.input_description);
                addressToEditText = findViewById(R.id.input_to_address);
                blockedHelmCheckBox = findViewById(R.id.input_blocked_helm);
                blockedBrakeCheckBox = findViewById(R.id.input_blocked_brake);
                damagedWheelsCheckBox = findViewById(R.id.input_damaged_wheels);
                transmissionTypeSpinner = findViewById(R.id.input_transmission_type);
                clearWaySpinner = findViewById(R.id.input_clear_way);
            }

            if (serviceName.equals("Подвоз топлива")) {
                LinearLayout fieldsGasoline = findViewById(R.id.fields_group_gasoline);
                fieldsGasoline.setVisibility(View.VISIBLE);
                noticeLabel.setText(R.string.notice_gasoline);
                gasolineTypeSpinner = findViewById(R.id.input_gasoline_type);
                gasolineVariantSpinner = findViewById(R.id.input_gasoline_variant);
                gasolineVolumeSpinner = findViewById(R.id.input_gasoline_volume);
            }

            if (serviceName.equals("Вскрытие замков")) {
                noticeLabel.setText(R.string.notice_lock);
            }

            if (serviceName.equals("Замена колеса")) {
                LinearLayout fieldsWheel= findViewById(R.id.fields_group_wheel);
                fieldsWheel.setVisibility(View.VISIBLE);
                spareWheelCheckBox = findViewById(R.id.input_spare_wheel);
                wheelSecretsCheckBox = findViewById(R.id.input_wheel_secrets);
            }

            if (serviceName.equals("Запуск двигателя от внешнего источника")) {
                LinearLayout fieldsBattery = findViewById(R.id.fields_group_battery);
                fieldsBattery.setVisibility(View.VISIBLE);
                voltageSpinner = findViewById(R.id.input_voltage);
                existedAccessCheckBox = findViewById(R.id.input_inside_access);
                openingHoodCheckBox = findViewById(R.id.input_hood_opening);
            }

            if (serviceName.equals("Подвоз топлива") || serviceName.equals("Вскрытие замков")) {
                noticeLabel.setVisibility(View.VISIBLE);
            } else {
                noticeLabel.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            /* open location picker */
            case R.id.action_from_map:
                try {
                    PlacePicker.IntentBuilder placePickerBuilder = new PlacePicker.IntentBuilder();
                    startActivityForResult(placePickerBuilder.build(ServiceActivity.this), REQUEST_FROM_PLACE_PICKER);
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.getMessage();
                } catch (GooglePlayServicesRepairableException e) {
                    e.getMessage();
                }
                break;

            case R.id.action_to_map:
                try {
                    PlacePicker.IntentBuilder placePickerBuilder = new PlacePicker.IntentBuilder();
                    startActivityForResult(placePickerBuilder.build(ServiceActivity.this), REQUEST_TO_PLACE_PICKER);
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.getMessage();
                } catch (GooglePlayServicesRepairableException e) {
                    e.getMessage();
                }
                break;

            /* send an order */
            case R.id.action_submit:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);

                if (!ServiceOrderHelper.checkInternetConnection(this)) {
                    builder.setMessage(R.string.dialog_no_internet).show();
                    return;
                }

                /* the simplest validation */
                if (ValidationHelper.validateName(nameEditText) && ValidationHelper.validatePhone(phoneEditText)) {
                    sendOrder();
                }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        /* if location is selected, put it into text input */
        if (REQUEST_FROM_PLACE_PICKER == requestCode || REQUEST_TO_PLACE_PICKER == requestCode) {
            if (resultCode == RESULT_OK) {
                Place address = PlacePicker.getPlace(this, data);
                String mapAddress = address.getAddress().toString();

                if (REQUEST_FROM_PLACE_PICKER == requestCode) {
                    TextInputEditText addressEditText = findViewById(R.id.input_from_address);
                    addressEditText.setText(mapAddress);
                } else {
                    TextInputEditText addressEditText = findViewById(R.id.input_to_address);
                    addressEditText.setText(mapAddress);
                }
            }
        }
    }

    private void sendOrder() {
        ServiceOrderHelper.disableSendButton(submitButton);

        final AlertDialog.Builder builder = new AlertDialog.Builder(ServiceActivity.this);

        /* collect all request data to HashMap */
        RequestParams params = new RequestParams();

        params.put("service", serviceName);
        params.put("addressFrom", addressFromEditText.getText().toString());
        params.put("name", nameEditText.getText().toString());
        params.put("phone", phoneEditText.getText().toString());
        params.put("carMake", carMakeEditText.getText().toString());
        params.put("carModel", carModelEditText.getText().toString());

        /* add fields from preferences */
        String carNumber = profilePreferences.getString("profileCarNumber", "");
        params.put("carNumber", carNumber);

        Boolean profileMembership = profilePreferences.getBoolean("profileMembership", false);
        if (profileMembership) {
            String profileCardNumber = profilePreferences.getString("profileCardNumber", "");
            params.put("cardNumber", profileCardNumber);
        }

        /* add fields depend on selected service */
        if ( serviceName.equals("Эвакуация автомобиля")) {
            params.put("addressTo", addressToEditText.getText().toString());
            params.put("description", descriptionEditText.getText().toString());
            params.put("transmissionType", transmissionTypeSpinner.getSelectedItem().toString());
            params.put("isWayClear", clearWaySpinner.getSelectedItem().toString());
            params.put("isHelmBlocked", (blockedHelmCheckBox.isChecked() ? "Да" : "Нет"));
            params.put("isBrakeBlocked", (blockedBrakeCheckBox.isChecked() ? "Да" : "Нет"));
            params.put("isWheelsDamaged", (damagedWheelsCheckBox.isChecked() ? "Да" : "Нет"));
        }

        if (serviceName.equals("Подвоз топлива")) {
            params.put("gasolineType", gasolineTypeSpinner.getSelectedItem().toString());
            params.put("gasolineVariant", gasolineVariantSpinner.getSelectedItem().toString());
            params.put("gasolineVolume", gasolineVolumeSpinner.getSelectedItem().toString());
        }

        if (serviceName.equals("Запуск двигателя от внешнего источника")) {
            params.put("current", voltageSpinner.getSelectedItem().toString());
            params.put("isExistAccess", (existedAccessCheckBox.isChecked() ? "Да" : "Нет"));
            params.put("isHoodOpening", (openingHoodCheckBox.isChecked() ? "Да" : "Нет"));
        }

        if (serviceName.equals("Замена колеса")) {
            params.put("isSpareWheel", (spareWheelCheckBox.isChecked() ? "Да" : "Нет"));
            params.put("isWheelSecrets", (wheelSecretsCheckBox.isChecked() ? "Да" : "Нет"));
        }

        ServiceOrderHelper.send(params, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                builder.setMessage(R.string.request_success).show();
                ServiceOrderHelper.enableSendButton(submitButton);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                builder.setMessage(R.string.request_fail + ". Текст ошибки: " + new String(errorResponse)).show();
                ServiceOrderHelper.enableSendButton(submitButton);
            }
        });
    }
}