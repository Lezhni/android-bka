package by.bka.application;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.bumptech.glide.Glide;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import by.bka.application.helpers.ServiceOrderHelper;
import by.bka.application.helpers.ValidationHelper;
import cz.msebera.android.httpclient.Header;
import io.fabric.sdk.android.Fabric;

public class AdditionalServicesActivity extends AppCompatActivity implements Spinner.OnItemSelectedListener, View.OnClickListener {

    private static final int REQUEST_CAMERA = 1;
    /*private static final int REQUEST_GALLERY = 2;*/
    private static final int REQUEST_FROM_PLACE_PICKER = 3;
    private static final int PERMISSION_CAMERA = 100;

    private SharedPreferences profilePreferences;

    private String insurancePhotoPath;
    private String[] additionalServicesList;

    File photoFile = null;
    Uri photoURI = null;

    private ImageView insuranceImageView;
    private Button submitButton;
    private AutoCompleteTextView carMakeEditText;
    private AppCompatSpinner additionalServiceSpinner, carClassSpinner;
    private EditText addressFromEditText, nameEditText, phoneEditText, carModelEditText, rentDurationEditText;
    private LinearLayout fieldsCarChange, fieldsInsurance, fieldsSoberDriver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_additional_services);

        fieldsCarChange = findViewById(R.id.fields_group_car_change);
        fieldsInsurance = findViewById(R.id.fields_group_insurance);
        fieldsSoberDriver = findViewById(R.id.fields_group_sober);
        insuranceImageView = findViewById(R.id.insurance_photo);

        addressFromEditText = findViewById(R.id.input_from_address);
        nameEditText = findViewById(R.id.input_name);
        phoneEditText = findViewById(R.id.input_phone);
        carMakeEditText = findViewById(R.id.input_car_make);
        carModelEditText = findViewById(R.id.input_car_model);
        rentDurationEditText = findViewById(R.id.input_rent_duration);

        additionalServiceSpinner = findViewById(R.id.input_additional_service);
        carClassSpinner = findViewById(R.id.input_car_class);

        Button cameraButton = findViewById(R.id.action_camera);
        cameraButton.setOnClickListener(this);

        /*Button galleryButton = findViewById(R.id.action_gallery);
        galleryButton.setOnClickListener(this);*/

        Button mapFromButton = findViewById(R.id.action_from_map);
        mapFromButton.setOnClickListener(this);

        submitButton = findViewById(R.id.action_submit);
        submitButton.setOnClickListener(this);

        /* fill profile's field with data from preferences */
        profilePreferences = PreferenceManager.getDefaultSharedPreferences(this);

        String profileName = profilePreferences.getString("profileName", "");
        nameEditText.setText(profileName);

        String profilePhone = profilePreferences.getString("profilePhone", "+375");
        phoneEditText.setText(profilePhone);

        /* fill additional services' input with values */
        additionalServicesList = getResources().getStringArray(R.array.additionalServices);
        additionalServiceSpinner.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch(additionalServicesList[position]) {
            case "Подменный автомобиль":
                fieldsCarChange.setVisibility(View.VISIBLE);
                fieldsInsurance.setVisibility(View.GONE);
                fieldsSoberDriver.setVisibility(View.GONE);
                break;
            case "Трезвый водитель":
                fieldsSoberDriver.setVisibility(View.VISIBLE);
                fieldsCarChange.setVisibility(View.GONE);
                fieldsInsurance.setVisibility(View.GONE);
                break;
            case "Страхование":
                fieldsInsurance.setVisibility(View.VISIBLE);
                fieldsCarChange.setVisibility(View.GONE);
                fieldsSoberDriver.setVisibility(View.GONE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {}

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        /* if location is selected, put it into text input */
        if (REQUEST_FROM_PLACE_PICKER == requestCode) {
            if (resultCode == RESULT_OK) {
                Place address = PlacePicker.getPlace(this, data);
                CharSequence mapAddress = address.getAddress();
                if (null != mapAddress) {
                    addressFromEditText.setText(mapAddress);
                }
            }
        }

        if (REQUEST_CAMERA == requestCode) {
            if (resultCode == RESULT_OK) {
                Glide.with(this).load(insurancePhotoPath).into(insuranceImageView);
                Log.i("SELECTED_FILE", "File from camera: " + insurancePhotoPath);
            }
        }

        /*if (REQUEST_GALLERY == requestCode) {
            if (resultCode == RESULT_OK) {
            }
        }*/

    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (PERMISSION_CAMERA == requestCode) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                try {
                    photoFile = createImageFile();
                    photoURI = FileProvider.getUriForFile(this, "by.bka.application.fileprovider", photoFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(cameraIntent, REQUEST_CAMERA);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            /* pick photo from gallery */
            /*case R.id.action_gallery:
                Intent pickPhoto = new Intent(Intent.ACTION_GET_CONTENT);
                pickPhoto.addCategory(Intent.CATEGORY_OPENABLE);
                pickPhoto.setType("image/*");
                startActivityForResult(pickPhoto, REQUEST_GALLERY);
                break;*/

            /* take a new photo */
            case R.id.action_camera:
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CAMERA}, PERMISSION_CAMERA);
                } else {
                    try {
                        photoFile = createImageFile();
                        photoURI = FileProvider.getUriForFile(this, "by.bka.application.fileprovider", photoFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(cameraIntent, REQUEST_CAMERA);
                }
                break;

            /* open location picker */
            case R.id.action_from_map:
                try {
                    PlacePicker.IntentBuilder placePickerBuilder = new PlacePicker.IntentBuilder();
                    startActivityForResult(placePickerBuilder.build(AdditionalServicesActivity.this), REQUEST_FROM_PLACE_PICKER);
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.getMessage();
                } catch (GooglePlayServicesRepairableException e) {
                    e.getMessage();
                }
                break;

            /* send an order */
            case R.id.action_submit:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);

                if (!ServiceOrderHelper.checkInternetConnection(this)) {
                    builder.setMessage(R.string.dialog_no_internet).show();
                    return;
                }

                /* the simplest validation */
                if (ValidationHelper.validateName(nameEditText) && ValidationHelper.validatePhone(phoneEditText)) {
                    sendOrder();
                }
        }
    }

    private void sendOrder() {
        ServiceOrderHelper.disableSendButton(submitButton);

        final AlertDialog.Builder builder = new AlertDialog.Builder(AdditionalServicesActivity.this);
        String serviceName = additionalServiceSpinner.getSelectedItem().toString();

        /* collect all request data to HashMap */
        RequestParams params = new RequestParams();

        params.put("service", serviceName);
        params.put("addressFrom", addressFromEditText.getText().toString());
        params.put("name", nameEditText.getText().toString());
        params.put("phone", phoneEditText.getText().toString());

        /* add fields from preferences */
        String carNumber = profilePreferences.getString("profileCarNumber", "");
        params.put("carNumber", carNumber);

        Boolean profileMembership = profilePreferences.getBoolean("profileMembership", false);
        if (profileMembership) {
            String profileCardNumber = profilePreferences.getString("profileCardNumber", "");
            params.put("cardNumber", profileCardNumber);
        }

        if (serviceName.equals("Подменный автомобиль")) {
            params.put("carClass", carClassSpinner.getSelectedItem().toString());
            params.put("carMake", carMakeEditText.getText().toString());
            params.put("carModel", carModelEditText.getText().toString());
            params.put("rentDuration", rentDurationEditText.getText().toString());
        }

        File insuranceFile = new File(insurancePhotoPath);
        try {
            params.put("insurancePhoto", insuranceFile);
        } catch(Exception e) {
            e.printStackTrace();
        }

        ServiceOrderHelper.send(params, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                builder.setMessage(R.string.request_success).show();
                ServiceOrderHelper.enableSendButton(submitButton);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                builder.setMessage(R.string.request_fail + ". Текст ошибки: " + new String(errorResponse)).show();
                ServiceOrderHelper.enableSendButton(submitButton);
            }
        });
    }

    /* save temp photo from camera */
    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);

        insurancePhotoPath = image.getAbsolutePath();
        return image;
    }
}