package by.bka.application;

import android.os.Bundle;
import android.preference.ListPreference;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;

import com.crashlytics.android.Crashlytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import by.bka.application.helpers.AssetsHelper;
import io.fabric.sdk.android.Fabric;

public class ProfileActivity extends AppCompatPreferenceActivity {

    private List<String> carsMakes = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        addPreferencesFromResource(R.xml.pref_profile);

        final ListPreference profileCarMake = (ListPreference) findPreference("profileCarMake");

        /* parse JSON with makes/models */
        String carsJSON = AssetsHelper.getJSONFile(this, "carsList.json");
        try {
            JSONArray carsMakesJSON = new JSONArray(carsJSON);
            for (int i = 0; i < carsMakesJSON.length(); i++) {
                JSONObject carMakeJSON = carsMakesJSON.getJSONObject(i);
                String carMakeName = carMakeJSON.getString("brand");
                carsMakes.add(carMakeName);
            }

            /* fill ListPreference with cars' makes */
            setListPreferenceData(profileCarMake, carsMakes);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected static void setListPreferenceData(ListPreference lp, List<String> entries) {
        CharSequence[] entriesList = entries.toArray(new CharSequence[entries.size()]);
        lp.setEntries(entriesList);
        lp.setEntryValues(entriesList);
    }
}
